# AstroNvim User Configuration

Personal user configuration for [AstroNvim](https://github.com/AstroNvim/AstroNvim)
[Template](https://github.com/AstroNvim/user_example)

## 🛠️ Installation

#### 1. Make a backup of your current nvim and shared folder

```shell
mv ~/.config/nvim ~/.config/nvim.bak
mv ~/.local/share/nvim ~/.local/share/nvim.bak
```

#### 2. Clone AstroNvim

```shell
git clone https://github.com/AstroNvim/AstroNvim ~/.config/nvim
```

#### Clone the configuration repository

```shell
git clone git@gitlab.com:p.shir/astronvim_config.git ~/.config/nvim/lua/user
```

OR

```shell
git clone https://gitlab.com/p.shir/astronvim_config.git ~/.config/nvim/lua/user
```

#### Start Neovim

```shell
nvim
```

