return {
  -- You can also add new plugins here as well:
  -- Add plugins, the lazy syntax
  {
    -- Git conflicts highligth
    -- TODO: fix refresh problem for subprojects
    -- TODO: fix color problem
    -- [Link](https://github.com/akinsho/git-conflict.nvim)
    "akinsho/git-conflict.nvim",
    opts = {
      default_commands = true, -- disable commands created by this plugin
      default_mappings = false, -- disable mappings created by this plugin
      disable_diagnostics = true, -- This will disable the diagnostics in a buffer whilst it is conflicted
      list_opener = 'copen', -- command or function to open the conflicts list
      highlights = { -- They must have background color, otherwise the default color will be used
        incoming = 'DiffAdd',
        current = 'DiffText',
      }
    },
    lazy = false,
  },

  {
    -- Custom snippets
    "L3MON4D3/LuaSnip",
    config = function(plugin, opts)
      -- include the default astronvim config that calls the setup call
      require "plugins.configs.luasnip"(plugin, opts)
      -- load snippets paths
      require("luasnip.loaders.from_vscode").lazy_load {
        -- this can be used if your configuration lives in ~/.config/nvim
        -- if your configuration lives in ~/.config/astronvim, the full path
        -- must be specified in the next line
        paths = { "./lua/user/snippets" }
      }
    end,
  },

  {
    -- .env file support
    "ellisonleao/dotenv.nvim",
    opts = {
      enable_on_load = true, -- will load your .env file upon loading a buffer
      verbose = true, -- show error notification if .env file is not found and if .env is loaded
    },
    lazy = false,
    event = "VeryLazy",
  },
}
