  -- Customize heirline options (add venv info)
return {
  {
    "rebelot/heirline.nvim",
    opts = function(_, opts)
      local status = require("astronvim.utils.status")

      -- TODO: display venv name only for python project
      local actived_venv = function()
	      local venv_name = require("venv-selector").get_active_venv()
	      if venv_name ~= nil then
	        if venv_name == (vim.fn.getcwd() .. "/.venv") then
	          return ".venv"
	        else
		        return string.gsub(venv_name, ".*/pypoetry/virtualenvs/", "(poetry) ")
		      end
	      else
		      return "None"
	      end
      end

      local venv = {
	      {
		      provider = function() return  " 🭪 " .. actived_venv() .. " 🭨" end,
	      },
	      on_click = {
		      callback = function() vim.cmd.VenvSelect() end,
		      name = "heirline_statusline_venv_selector",
	      },
      }

      opts.statusline = { -- statusline
      hl = { fg = "fg", bg = "bg" },
      -- status.component.mode { mode_text = { padding = { left = 1, right = 1 } } }, -- add the mode text
      status.component.mode(),
      status.component.git_branch(),
      status.component.file_info { filetype = {}, filename = false, file_modified = false },
      status.component.git_diff(),
      status.component.diagnostics(),
      status.component.fill(),
      status.component.cmd_info(),
      status.component.fill(),
      status.component.lsp(),
      status.component.treesitter(),
      venv,
      status.component.nav(),
      status.component.mode { surround = { separator = "right" } },
      -- remove the 2nd mode indicator on the right
    }

    -- return the final configuration table
    return opts
  end,
},
}
