-- Customize nvim-dap-ui options
return {
  {
    "rcarriga/nvim-dap-ui",
    opts = function(_, opts)
    opts.layouts = { {
        elements = { {
            id = "scopes",
            size = 0.4
          }, {
            id = "stacks",
            size = 0.25
          }, {
            id = "breakpoints",
            size = 0.20
          }, {
            id = "watches",
            size = 0.15
          } },
        position = "left",
        size = 60
      }, {
        elements = { {
            id = "console",
            size = 0.6
          }, {
            id = "repl",
            size = 0.4
          } },
        position = "bottom",
        size = 10
      } }

    -- return the final configuration table
    return opts
  end,
},
}
