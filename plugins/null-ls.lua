return {
  "jose-elias-alvarez/null-ls.nvim",
  opts = function(_, config)
    -- config variable is the default configuration table for the setup function call
    local null_ls = require "null-ls"

    -- Check supported formatters and linters
    -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
    -- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
    config.sources = {
      -- Set a formatter
      -- null_ls.builtins.formatting.stylua,
      -- null_ls.builtins.formatting.prettier,

      -- Python tools
      -- null_ls.builtins.diagnostics.ruff, -- disable ruff, because ruff_lsp from astrocommunity used
      null_ls.builtins.diagnostics.mypy,
      null_ls.builtins.formatting.black,
      null_ls.builtins.formatting.isort,
    }
    return config -- return final config table
  end,
}
