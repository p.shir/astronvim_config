return {
  -- Configure AstroNvim updates
  updater = {
    remote = "origin", -- remote to use
    channel = "stable", -- "stable" or "nightly"
    version = "latest", -- "latest", tag name, or regex search like "v1.*" to only do updates before v2 (STABLE ONLY)
    branch = "nightly", -- branch name (NIGHTLY ONLY)
    commit = nil, -- commit hash (NIGHTLY ONLY)
    pin_plugins = nil, -- nil, true, false (nil will pin plugins on stable only)
    skip_prompts = false, -- skip prompts about breaking changes
    show_changelog = true, -- show the changelog after performing an update
    auto_quit = false, -- automatically quit the current session after a successful update
    remotes = { -- easily add new remotes to track
      --   ["remote_name"] = "https://remote_url.come/repo.git", -- full remote url
      --   ["remote2"] = "github_user/repo", -- GitHub user/repo shortcut,
      --   ["remote3"] = "github_user", -- GitHub user assume AstroNvim fork
    },
  },

  -- Set colorscheme to use
  colorscheme = "vscode",

  -- Diagnostics configuration (for vim.diagnostics.config({...})) when diagnostics are on
  diagnostics = {
    virtual_text = true,
    underline = true,
  },

  lsp = {
    -- customize lsp formatting options
    formatting = {
      -- control auto formatting on save
      format_on_save = {
        enabled = true, -- enable or disable format on save globally
        allow_filetypes = { -- enable format on save for specified filetypes only
          -- "go",
        },
        ignore_filetypes = { -- disable format on save for specified filetypes
          "python",
        },
      },
      disabled = { -- disable formatting capabilities for the listed language servers
        -- disable lua_ls formatting capability if you want to use StyLua to format your lua code
        -- "lua_ls",
      },
      timeout_ms = 1000, -- default format timeout
      -- filter = function(client) -- fully override the default formatting function
      --   return true
      -- end
    },
    -- enable servers that you already have installed without mason
    servers = {
      -- "pyright"
    },
  },

  -- Configure require("lazy").setup() options
  lazy = {
    defaults = { lazy = true },
    performance = {
      rtp = {
        -- customize default disabled vim plugins
        disabled_plugins = { "tohtml", "gzip", "matchit", "zipPlugin", "netrwPlugin", "tarPlugin" },
      },
    },
  },


  -- fix heirline chars
  heirline = {
    separators = {
      left = { "", "🭐 " }, -- separator for the left side of the statusline
      right = { " 🭅", "" }, -- separator for the right side of the statusline
      -- tab = { "🭋", "🭛" },
      -- breadcrumbs = " 🭬 ",
      -- path = " 🭬 ",
      breadcrumbs = " ⟩ ",
      path = " ⟩ ",
    }
  },
  -- This function is run last and is a good place to configuring
  -- augroups/autocommands and custom filetypes also this just pure lua so
  -- anything that doesn't fit in the normal config locations above can go here
  polish = function()
    -- Set up custom filetypes
    -- vim.filetype.add {
    --   extension = {
    --     foo = "fooscript",
    --   },
    --   filename = {
    --     ["Foofile"] = "fooscript",
    --   },
    --   pattern = {
    --     ["~/%.config/foo/.*"] = "fooscript",
    --   },
    -- }

    -- Enable use ${project_root}/.vscode/launch.json
    require('dap.ext.vscode').load_launchjs()

    -- Fix default icons for Variable and Constant
    require('aerial').setup {
        icons = {
          Constant = "Ⓒ ",
          Variable = "ⓥ ",
        },
        attach_mode = "global",
        backends = { "lsp", "treesitter", "markdown", "man" },
        disable_max_lines = vim.g.max_file.lines,
        disable_max_size = vim.g.max_file.size,
        layout = { min_width = 28 },
        show_guides = true,
        filter_kind = false,
        guides = {
          mid_item = "├ ",
          last_item = "└ ",
          nested_top = "│ ",
          whitespace = "  ",
        },
        keymaps = {
          ["[y"] = "actions.prev",
          ["]y"] = "actions.next",
          ["[Y"] = "actions.prev_up",
          ["]Y"] = "actions.next_up",
          ["{"] = false,
          ["}"] = false,
          ["[["] = false,
          ["]]"] = false,
        },
    }
    -- vim.cmd('set signcolumn=auto:3') -- set signcolumn autoresize up to 3 (see options.lua)
    -- vim.g.diagnostics_mode = 0 -- disable diagnostic sidebar sign (see options.lua)
    -- vim.fn.sign_define('DapBreakpoint', { text='🧐', texthl='Error'}) -- set breakpoints color
    vim.fn.sign_define('DapBreakpoint', { text='', texthl='Error'}) -- set breakpoints color
    vim.api.nvim_set_hl(0, 'MarkSignHL', { ctermbg = 0, fg = '#5def73' }) -- set marks color
    -- toggle lsp_lines (disable linter errors under line)
    vim.diagnostic.config {
      virtual_text = not require("lsp_lines").toggle(),
    }
    -- "linux-cultist/venv-selector.nvim",
    -- [default config](https://github.com/linux-cultist/venv-selector.nvim/blob/main/lua/venv-selector/config.lua)
    require('venv-selector').setup {
      opts = {},
      -- name = {"venv", ".venv"},
      name = {".venv"},
      -- keys = { { "<leader>lv", "<cmd>:VenvSelect<cr>", desc = "Select VirtualEnv" } },
      dap_enabled = true,
    }
    -- create autocmd to load cached venv
    vim.api.nvim_create_autocmd("VimEnter", {
      desc = "Auto select virtualenv Nvim open",
      pattern = "*",
      callback = function()
        local venv = vim.fn.findfile("pyproject.toml", vim.fn.getcwd() .. ";")
        if venv ~= "" then
          require("venv-selector").retrieve_from_cache()
        end
      end,
      once = true,
    })
  end,
}
