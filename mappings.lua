-- Mapping data with "desc" stored directly by vim.keymap.set().
--
-- Please use this mappings table to set keyboard mapping since this is the
-- lower level configuration and more robust one. (which-key will
-- automatically pick-up stored data by this setting.)
return {
  -- first key is the mode
  n = {
    -- second key is the lefthand side of the map
    -- mappings seen under group name "Buffer"
    ["<F3>"] = { "<cmd>Neotree toggle<cr>", desc = "Toggle Explorer" },
    ["<leader>bn"] = { "<cmd>tabnew<cr>", desc = "New tab" },
    ["<leader>bD"] = {
      function()
        require("astronvim.utils.status").heirline.buffer_picker(function(bufnr) require("astronvim.utils.buffer").close(bufnr) end)
      end,
      desc = "Pick to close",
    },
    -- tables with the `name` key will be registered with which-key if it's installed
    -- this is useful for naming menus
    ["<leader>b"] = { name = "Buffers" },
    -- quick save
    -- ["<C-s>"] = { ":w!<cr>", desc = "Save File" },  -- change description but the same command

    -- Trouble configuration
    ["<leader>lt"] = {
      function()
        require("trouble").toggle()
      end,
      desc = "Toggle Trouble window",
    },

    -- Open .vscode/launch.json
    ["<leader>dl"] = {
      ":e .vscode/launch.json <cr>",
      desc = "Open launch.json",
    },

    -- nvim-bqf
    ["<leader>lq"] = {
      -- https://github.com/kevinhwang91/nvim-bqf/issues/35
      function()
        local qf_open = false
        for _, win in pairs(vim.fn.getwininfo()) do
          if win["quickfix"] == 1 then
            qf_open = true
          end
        end
        if qf_open == true then
          vim.cmd("cclose")
          return
        end
        if not vim.tbl_isempty(vim.fn.getqflist()) then
          vim.cmd("copen")
        end
        if vim.b.bqf_enabled then
          vim.api.nvim_feedkeys([['"]], 'im', false)
        end
      end,
      desc = "Toggle nvim-bqf window",
    },

    -- git-conflict
    ["<leader>gm"] = { name = "Merge conflict" },
    ["<leader>gmo"] = { "<cmd>GitConflictChooseOurs<cr>", desc = "Choice ours" },
    ["<leader>gmt"] = { "<cmd>GitConflictChooseTheirs<cr>", desc = "Choice theirs" },
    ["<leader>gmb"] = { "<cmd>GitConflictChooseBoth<cr>", desc = "Choice both" },
    ["<leader>gmN"] = { "<cmd>GitConflictChooseNone<cr>", desc = "Choice none" },
    ["<leader>gml"] = { "<cmd>GitConflictListQf<cr>", desc = "List qf" },
    ["<leader>gmn"] = { "<cmd>GitConflictNextConflict<cr>", desc = "Next conflict" },
    ["<leader>gmp"] = { "<cmd>GitConflictPrevConflict<cr>", desc = "Prev conflict" },

    ["<Tab>"] = { function() require("astronvim.utils.buffer").nav(vim.v.count > 0 and vim.v.count or 1) end, desc = "Next buffer" },
    ["<S-Tab>"] = { function() require("astronvim.utils.buffer").nav(-(vim.v.count > 0 and vim.v.count or 1)) end, desc = "Previous buffer" },
    ["<leader><leader>"] = { function() require("astronvim.utils.buffer").prev() end, desc = "Previous opened buffer" },

    -- fix Neotree switch
    ["<leader>e"] = { "<cmd>Neotree toggle last<cr>", desc = "Toggle Neotree" },
    ["<leader>o"] = {
      function()
        if vim.bo.filetype == "neo-tree" then
          vim.cmd.wincmd "p"
        else
          vim.cmd("Neotree focus last")
        end
      end,
      desc = "Toggle Explorer Focus",
    },

    -- diagnostic toggle
    ["<leader>ll"] = {
      function()
        if vim.diagnostic.is_disabled() then
          vim.diagnostic.enable()
        else
          vim.diagnostic.disable()
        end
      end,
      desc = "Toggle diagnostic"
    },
    f = {
      -- function() require('hop').hint_char1({ direction = directions.AFTER_CURSOR, current_line_only = true }) end,
      function() require('hop').hint_char1() end,
      desc = "Hop: find char1",
    }
  },
  t = {
    -- setting a mapping to false will disable it
    -- ["<esc>"] = false,
  },
}
